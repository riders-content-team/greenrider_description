#include "LinearPlugin.hh"

using namespace gazebo;
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(LinearPlugin)

// Constructor
LinearPlugin::LinearPlugin() : ModelPlugin()
{
  this->object_gripped = false;
  this->joint_state = false;
  this->grip_state = false;
}

// Destructor
LinearPlugin::~LinearPlugin()
{
}

void LinearPlugin::Load(physics::ModelPtr model, sdf::ElementPtr _sdf)
{
  this->model = model;
  this->world = this->model->GetWorld();
  this->model->SetGravityMode(0);
  //this->world->ResetEntities(gazebo::physics::Base::MODEL);
  //this->world->Reset();

  this->robot_namespace = this->model->GetName();

  //Get wheel joints
  if (!_sdf->HasElement("joint_name_x"))
  {
    this->joint_name_x = "joint_name_x";
  } else {
    this->joint_name_x = _sdf->Get<std::string>("joint_name_x");
  }

  if (!_sdf->HasElement("joint_name_y"))
  {
    this->joint_name_y = "joint_name_y";
  } else {
    this->joint_name_y = _sdf->Get<std::string>("joint_name_y");
  }

  if (!_sdf->HasElement("joint_name_z"))
  {
    this->joint_name_z = "joint_name_z";
  } else {
    this->joint_name_z = _sdf->Get<std::string>("joint_name_z");
  }

  if (!_sdf->HasElement("torque"))
  {
    this->torque = 5;
  } else {
    this->torque = _sdf->Get<double>("torque");
  }

  if (!_sdf->HasElement("updateRate"))
  {
    this->update_rate = 100.0;
  } else {
    this->update_rate = _sdf->Get<double>("updateRate");
  }

  // Initialize update rate stuff
  if (this->update_rate > 0.0) {
    this->update_period = 1.0 / this->update_rate;
  } else {
    this->update_period = 0.0;
  }

  //Get Contact Sensor
  if (_sdf->HasElement("contact_sensor")){
    std::string contact_sensor_name = _sdf->Get<std::string>("contact_sensor");
    // Get sensor Pointer by Name
    this->contact_sensor = std::dynamic_pointer_cast<sensors::ContactSensor>(
       sensors::SensorManager::Instance()->GetSensor(contact_sensor_name));
    //Check for Sensor
    if (!this->contact_sensor)
    {
      std::cout << "Couldn't find sensor [" << contact_sensor_name
                << "]" << '\n';
      return;
    }
    // Set Contact Sensor Active
    // This Must be Called
    this->contact_sensor->SetActive(true);

  }else{
    std::cout << "No inside sensor name entered" << '\n';
  }

  this->last_update_time = this->world->SimTime().Double();

  // Initialize velocity stuff for x
  this->joint_pose_x = 0;
  this->lifter_joint_x = this->model->GetJoint(this->joint_name_x);
  this->lower_limit_x = this->lifter_joint_x->LowerLimit(0);
  this->upper_limit_x = this->lifter_joint_x->UpperLimit(0);

  if (!this->lifter_joint_x) {
    char error[200];
    snprintf(error, 200,
        "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get left front hinge joint named \"%s\"",
        this->robot_namespace.c_str(), this->joint_name_x.c_str());
    gzthrow(error);
  }

  this->lifter_joint_x->SetParam("fmax", 0, torque);

  // Initialize velocity stuff for y
  this->joint_pose_y = 0;
  this->lifter_joint_y = this->model->GetJoint(this->joint_name_y);
  this->lower_limit_y = this->lifter_joint_y->LowerLimit(0);
  this->upper_limit_y = this->lifter_joint_y->UpperLimit(0);

  if (!this->lifter_joint_y) {
    char error[200];
    snprintf(error, 200,
        "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get left front hinge joint named \"%s\"",
        this->robot_namespace.c_str(), this->joint_name_y.c_str());
    gzthrow(error);
  }

  this->lifter_joint_y->SetParam("fmax", 0, torque);

  // Initialize velocity stuff for z
  this->joint_pose_z = 0;
  this->lifter_joint_z = this->model->GetJoint(this->joint_name_z);
  this->lower_limit_z = this->lifter_joint_z->LowerLimit(0);
  this->upper_limit_z = this->lifter_joint_z->UpperLimit(0);

  if (!this->lifter_joint_z) {
    char error[200];
    snprintf(error, 200,
        "GazeboRosSkidSteerDrive Plugin (ns = %s) couldn't get left front hinge joint named \"%s\"",
        this->robot_namespace.c_str(), this->joint_name_z.c_str());
    gzthrow(error);
  }

  this->lifter_joint_z->SetParam("fmax", 0, torque);

  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized())
  //if(true)
  {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, this->robot_namespace,
      ros::init_options::NoSigintHandler);
  }

  // Create our ROS node. This acts in a similar manner to
  // the Gazebo node
  this->rosNode.reset(new ros::NodeHandle(this->robot_namespace));
  this->command_topic = "/" + this->robot_namespace + "/set_pose";
  ros::SubscribeOptions cso = ros::SubscribeOptions::create<geometry_msgs::Point>(
      this->command_topic,
      1,
      std::bind(&LinearPlugin::SpeedCallback, this, std::placeholders::_1),
      ros::VoidConstPtr(),
      NULL);

  this->cmd_vel_subscriber = this->rosNode->subscribe(cso);

  this->grip_topic = "/" + this->robot_namespace + "/grip_state";
  ros::SubscribeOptions gso = ros::SubscribeOptions::create<std_msgs::Bool>(
      this->grip_topic,
      1,
      std::bind(&LinearPlugin::GripCallback, this, std::placeholders::_1),
      ros::VoidConstPtr(),
      NULL);

  this->gripper_subscriber = this->rosNode->subscribe(gso);

  this->worldConnection = event::Events::ConnectWorldUpdateBegin(
          std::bind(&LinearPlugin::Update, this));

  this->resetConnection = event::Events::ConnectWorldReset(
          std::bind(&LinearPlugin::OnReset, this));

  this->resetConnection = event::Events::ConnectDeleteEntity (
          std::bind(&LinearPlugin::OnModelDelete, this));
}

void LinearPlugin::OnReset()
{
  //this->model->RemoveJoint("factory_joint");
  //this->model->RemoveJoint("gripped_joint");
  this->joint_state = false;
}

void LinearPlugin::OnModelDelete()
{
  //this->model->RemoveJoint("factory_joint");
  //this->model->RemoveJoint("gripped_joint");
  this->joint_state = false;
}

void LinearPlugin::GripCallback(const std_msgs::Bool::ConstPtr& msg)
{
  this->grip_state = msg->data;
}


void LinearPlugin::SpeedCallback(const geometry_msgs::Point::ConstPtr& msg)
{
  // handling x pose
  this->joint_pose_x = msg->x;
  if (this->joint_pose_x >= this->upper_limit_x){
      this->joint_pose_x = this->upper_limit_x;
  }
  else if (this->joint_pose_x <= this->lower_limit_x){
      this->joint_pose_x = this->lower_limit_x;
  }
  // handling y pose
  this->joint_pose_y = msg->y;
  if (this->joint_pose_y >= this->upper_limit_y){
      this->joint_pose_y = this->upper_limit_y;
  }
  else if (this->joint_pose_y <= this->lower_limit_y){
      this->joint_pose_y = this->lower_limit_y;
  }
  // handling z pose
  this->joint_pose_z = msg->z;
  if (this->joint_pose_z >= this->upper_limit_z){
      this->joint_pose_z = this->upper_limit_z;
  }
  else if (this->joint_pose_z <= this->lower_limit_z){
      this->joint_pose_z = this->lower_limit_z;
  }
  //std::cout << msg->data << '\n';
}


void LinearPlugin::Update()
{
  //if(!this->joint_state)
  if(false)
  {
    std::string factory_name = "factory";
    physics::Model_V world_models = this->world->Models();
    physics::ModelPtr factory_model;
    for(int i = 0; i<world_models.size(); i++)
    {
      if(!factory_name.compare(world_models[i]->GetName()))
      {
        //std::cout << world_models[i]->GetName() << '\n';
        factory_model = world_models[i];
        break;
      }
    }

    std::cout << factory_model->GetName() << '\n';
    physics::LinkPtr parent_link = factory_model->GetLink("factory_link");
    physics::LinkPtr child_link = this->model->GetLink("frame_link");
    this->model->CreateJoint("factory_joint", "fixed", parent_link, child_link);
    this->joint_state = true;
  }
  double current_time = this->world->SimTime().Double();
  double seconds_since_last_update = current_time - this->last_update_time;

  //std::cout << this->grip_state << '\n';
  //std::cout << this->object_gripped << '\n';
  //std::cout << "=======================" << '\n';

  if (seconds_since_last_update > this->update_period){

    //handling x position
    this->joint_position_x = this->lifter_joint_x->Position(0);

    if (this->joint_pose_x - this->joint_position_x >= 0.005){
        this->joint_position_x += 0.005;
    }
    else if (this->joint_pose_x - this->joint_position_x < -0.005){
        this->joint_position_x -= 0.005;
    }
    else{
        this->joint_position_x = this->joint_pose_x;
    }
    this->lifter_joint_x->SetPosition(0, this->joint_position_x, false);

    //handling y position
    this->joint_position_y = this->lifter_joint_y->Position(0);

    if (this->joint_pose_y - this->joint_position_y >= 0.005){
        this->joint_position_y += 0.005;
    }
    else if (this->joint_pose_y - this->joint_position_y < -0.005){
        this->joint_position_y -= 0.005;
    }
    else{
        this->joint_position_y = this->joint_pose_y;
    }
    this->lifter_joint_y->SetPosition(0, this->joint_position_y, false);

    //handling z position
    this->joint_position_z = this->lifter_joint_z->Position(0);

    if (this->joint_pose_z - this->joint_position_z >= 0.005){
        this->joint_position_z += 0.005;
    }
    else if (this->joint_pose_z - this->joint_position_z < -0.005){
        this->joint_position_z -= 0.005;
    }
    else{
        this->joint_position_z = this->joint_pose_z;
    }
    this->lifter_joint_z->SetPosition(0, this->joint_position_z, false);

    this->last_update_time = this->world->SimTime().Double();
  }

  std::string contact_model;
  contact_model = this->GetContactModel();
  physics::ModelPtr touch_model;
  if (contact_model.size() > 0) {

      touch_model = this->world->ModelByName(contact_model);
      /*
      for (int i = 0; i < contact_models.size(); ++i){
        if(!contact_models[i].compare("greenrider"))
        {
          std::cout << "non greenrider" << '\n';
          touch_model = this->world->ModelByName(contact_models[i]);
          break;
        }
      }
      */


      int joint_count = this->model->GetJointCount();
      //std::cout << joint_count << '\n';
      if(this->grip_state && (joint_count <= 4))
      {
        std::cout << touch_model->GetName() << '\n';
        physics::Link_V touch_links = touch_model->GetLinks();
        physics::LinkPtr child_link = touch_links[0];
        physics::LinkPtr parent_link = this->model->GetLink("claw_link");
        this->gripped_collision = child_link->GetCollision("object");
        if (this->gripped_collision)
        {
          std::cout << "removed collision" << '\n';
          this->gripped_collision->SetCollision(false);
        }
        //physics::LinkPtr child_link = touch_model->GetLink("link");
        std::cout << parent_link << '\n';
        std::cout << child_link << '\n';
        this->model->CreateJoint("gripped_joint", "fixed", parent_link, child_link);
        touch_model->SetLinearVel(ignition::math::Vector3d(0,0,0));
        this->joint_pose_x = this->lifter_joint_x->Position(0);
        this->joint_pose_y = this->lifter_joint_y->Position(0);
        this->joint_pose_z = this->lifter_joint_z->Position(0);
        std::cout << "gripped" << '\n';
        this->object_gripped = true;
      }
  }
  else if(!this->grip_state && this->object_gripped)
  {
    this->model->RemoveJoint("gripped_joint");
    this->object_gripped = false;
    std::cout << "object released" << '\n';
    if (this->gripped_collision)
    {
      std::cout << "readded collision" << '\n';
      this->gripped_collision->SetCollision(true);
    }
  }
}


std::string LinearPlugin::GetContactModel(){
  msgs::Contacts contacts;
  contacts = this->contact_sensor->Contacts();
  std::string touching_model = "";
  for (int i = 0; i < contacts.contact_size(); ++i){
    touching_model = contacts.contact(
       i).collision1().substr(0, contacts.contact(
         i).collision1().find("::"));

    std::cout << "============================" << '\n';
    std::cout << touching_model << '\n';
    std::cout << touching_model.compare(this->model->GetName()) << '\n';
    if (touching_model !=(this->model->GetName()))
    {
      return touching_model;
    }

    touching_model = contacts.contact(
       i).collision2().substr(0, contacts.contact(
         i).collision2().find("::"));

    std::cout << touching_model << '\n';
    std::cout << touching_model.compare(this->model->GetName()) << '\n';

    std::cout << "========================" << '\n';
    if (touching_model != (this->model->GetName()))
    {
      return touching_model;
    }
  }
  return "";
}
