#ifndef LIFTER_PLUGIN_HH
#define LIFTER_PLUGIN_HH

#include "gazebo/common/common.hh"
#include "ros/ros.h"
#include <ignition/math/Pose3.hh>
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/physics/PhysicsIface.hh"
#include <gazebo/sensors/sensors.hh>

#include <std_msgs/Float64.h>
#include <geometry_msgs/Point.h>
#include "std_msgs/Bool.h"


namespace gazebo
{
  class GAZEBO_VISIBLE LinearPlugin : public ModelPlugin
  {
    // brief Constructor
    public: LinearPlugin();

    // brief Destructor
    public: ~LinearPlugin();

    // brief Load the plugin
    // param take in SDF root element
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);

    public: virtual void SpeedCallback(const geometry_msgs::Point::ConstPtr& msg);

    public: virtual void GripCallback(const std_msgs::Bool::ConstPtr& msg);

    private: virtual void OnReset();

    private: virtual void OnModelDelete();

    public: virtual std::string GetContactModel();

    private: virtual void Update();

    private: physics::ModelPtr model;
    private: physics::WorldPtr world;
    private: physics::CollisionPtr gripped_collision;

    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection, resetConnection;

    private: std::string joint_name_x, joint_name_y, joint_name_z;
    private: std::string command_topic, robot_namespace, grip_topic;

    private: double torque, update_rate, update_period, last_update_time;

    private: double joint_pose_x, lower_limit_x, upper_limit_x, joint_position_x;
    private: double joint_pose_y, lower_limit_y, upper_limit_y, joint_position_y;
    private: double joint_pose_z, lower_limit_z, upper_limit_z, joint_position_z;
    private: physics::JointPtr lifter_joint_x, lifter_joint_y, lifter_joint_z;

    private: bool grip_state, object_gripped, joint_state;

    protected: std::unique_ptr<ros::NodeHandle> rosNode;
    private: ros::Subscriber cmd_vel_subscriber, gripper_subscriber;
    private: sensors::ContactSensorPtr contact_sensor;
  };
}
#endif
